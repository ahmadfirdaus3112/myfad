@php
    $config = [
        'appName' => config('app.name'),
        'locale' => $locale = app()->getLocale(),
        'locales' => config('app.languages'),

    ];

@endphp
    <!doctype html>
<html lang="{{ session('locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Firdaus">
    <meta name="description" content="Laravel Vue SPA">

    <title>{{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    {{-- Global configuration object --}}
    <script>window.config = @json($config);</script>
    <script>
        window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'siteName'  => config('app.name'),
                'apiDomain' => config('app.url').'/api'
            ]) !!}
	</script>

</head>
<body>
<div class="wrapper" id="app">

    {{--vue component for topnavbar--}}
    <vue-topmenu></vue-topmenu>
    <vue-sidebar></vue-sidebar>
{{--------------------}}

<!-- Main Sidebar Container -->
{{--<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
        <img src="./img/logo.png" alt="myfad Logo" class="brand-image"
             style="opacity: .8">
        <span class="brand-text font-weight-light">myfAd<small><sup>{{ str_replace('_', '-', app()->getLocale()) }}</sup></small></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="./img/profile.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">
                    <p>{{Auth::user()->type}}</p>
                </a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <router-link to="/dashboard" class="nav-link">
                        <i class="nav-icon fas fa-10x fa-tachometer-alt blue"></i>
                        <p>
                            {{ __('messages.dashboard') }}
                        </p>
                    </router-link>
                </li>
                --}}{{--subscriber--}}{{--
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-cog cyan"></i>
                        <p>
                            Subscriber
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/subscriber_profile" class="nav-link">
                                <i class="nav-icon fas fa-user purple"></i>
                                <p>
                                    {{ __('messages.profile') }}
                                </p>
                            </router-link>
                        </li>

                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/subscriber_network" class="nav-link">
                                <i class="nav-icon fas fa-sitemap purple"></i>
                                <p>
                                    Network
                                </p>
                            </router-link>
                        </li>

                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/subscriber_referral" class="nav-link">
                                <i class="nav-icon fas fa-hashtag purple"></i>
                                <p>
                                    Referral Page
                                </p>
                            </router-link>
                        </li>

                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/subscriber_earning" class="nav-link">
                                <i class="nav-icon fas fa-hashtag purple"></i>
                                <p>
                                    Earning
                                </p>
                            </router-link>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/subscriber_cashout" class="nav-link">
                                <i class="nav-icon fas fa-hashtag purple"></i>
                                <p>
                                    Cashout
                                </p>
                            </router-link>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/subscriber_ads" class="nav-link">
                                <i class="nav-icon fas fa-hashtag purple"></i>
                                <p>
                                    Ads
                                </p>
                            </router-link>
                        </li>
                    </ul>
                </li>

                --}}{{--merchant--}}{{--
                @if(!$user_merchant)
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog cyan"></i>
                            <p>
                                Merchant
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/merchant_register" class="nav-link">
                                    <i class="nav-icon fas fa-sitemap purple"></i>
                                    <p>
                                        Register Merchant
                                    </p>
                                </router-link>
                            </li>

                        </ul>

                    </li>
                @else
                    @foreach($merchant as $admin)
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-cog green"></i>
                                <p>
                                    {{$admin->mct_name}}
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <router-link to="/profile?mct_id={{$admin->mct_id}}"
                                                 class="nav-link">
                                        <i class="nav-icon fas fa-user orange"></i>
                                        <p>
                                            {{ __('messages.profile') }}
                                        </p>
                                    </router-link>
                                </li>
                            </ul>
                            --}}{{--<ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <router-link to="/merchant_admin?mct_id={{$admin->merchant['mct_id']}}"
                                                 class="nav-link">
                                        <i class="nav-icon fas fa-user orange"></i>
                                        <p>
                                            Account Administrator
                                        </p>
                                    </router-link>
                                </li>

                            </ul>--}}{{--
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <router-link to="/merchant_page?mct_id={{$admin->mct_id}}"
                                                 class="nav-link">
                                        <i class="nav-icon fas fa-user orange"></i>
                                        <p>
                                            Page Management
                                        </p>
                                    </router-link>
                                </li>

                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <router-link to="/merchant_ads?mct_id={{$admin->mct_id}}"
                                                 class="nav-link">
                                        <i class="nav-icon fas fa-user orange"></i>
                                        <p>
                                            Ads Management
                                        </p>
                                    </router-link>
                                </li>

                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <router-link to="/merchant_contest?mct_id={{$admin->mct_id}}"
                                                 class="nav-link">
                                        <i class="nav-icon fas fa-user orange"></i>
                                        <p>
                                            Contest Ads
                                        </p>
                                    </router-link>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <router-link to="/merchant_token?mct_id={{$admin->mct_id}}"
                                                 class="nav-link">
                                        <i class="nav-icon fas fa-user orange"></i>
                                        <p>
                                            Token
                                        </p>
                                    </router-link>
                                </li>
                            </ul>
                        </li>
                    @endforeach

                @endif


                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-cog green"></i>
                        <p>
                            {{ __('messages.management') }}
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/merchant_admin"
                                         class="nav-link">
                                <i class="nav-icon fas fa-user orange"></i>
                                <p>
                                    Account Administrator
                                </p>
                            </router-link>
                        </li>

                    </ul>
                    --}}{{--<ul class="nav nav-treeview">
                        <li class="nav-item">
                            <router-link to="/users" class="nav-link">
                                <i class="fas fa-users nav-icon"></i>
                                <p>{{ __('messages.user') }}</p>
                            </router-link>
                        </li>

                    </ul>--}}{{--
                </li>
                @can('isReviewer')
                    --}}{{--Reviewer--}}{{--
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog green"></i>
                            <p>
                                Reviewer
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/reviewer_inbox"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        Inbox
                                    </p>
                                </router-link>
                                <router-link to="/reviewer_new_ads"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        New Ads
                                    </p>
                                </router-link>
                                <router-link to="/reviewer_verified_ads"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        Verified Ads
                                    </p>
                                </router-link>
                            </li>
                        </ul>
                    </li>
                @endcan
                @can('isAdmin')
                    --}}{{--Reviewer--}}{{--
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog green"></i>
                            <p>
                                Reviewer
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/reviewer_inbox"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        Inbox
                                    </p>
                                </router-link>
                                <router-link to="/reviewer_new_ads"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        New Ads
                                    </p>
                                </router-link>
                                <router-link to="/reviewer_verified_ads"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        Verified Ads
                                    </p>
                                </router-link>
                            </li>
                        </ul>
                    </li>
                    --}}{{--Admin--}}{{--
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog green"></i>
                            <p>
                                Administrator
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/merchant_admin"
                                             class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>
                                        test
                                    </p>
                                </router-link>
                            </li>
                        </ul>
                    </li>

                    --}}{{--<li class="nav-item">
                          <router-link to="/developer" class="nav-link">
                              <i class="nav-icon fas fa-cogs"></i>
                              <p>
                                  Developer
                              </p>
                          </router-link>
                   </li>--}}{{--
                @endcan

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                        <i class="nav-icon fa fa-power-off red"></i>
                        <p>
                            {{ __('messages.logout') }}
                        </p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
--}}
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
                {{--router view to load vue component--}}
                <router-view :key="$route.fullPath"></router-view>
                <vue-progress-bar></vue-progress-bar>

        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <vue-footer></vue-footer>

</div>
@auth
    <script>
        window.user = @json(auth()->user())
    </script>
@endauth
<script src="{{ mix('/js/app.js') }}"></script>

</body>
</html>

