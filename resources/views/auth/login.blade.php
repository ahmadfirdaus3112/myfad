<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name') }}</title>

	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="new_login/images/icons/favicon.ico"/>
	<!--===============================================================================================-->

	<!--===============================================================================================-->


	<!--===============================================================================================-->

	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="new_login/css/util.css">
	<link rel="stylesheet" type="text/css" href="new_login/css/main.css">
	<!--===============================================================================================-->
</head>
<body>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<div class="login100-pic js-tilt" data-tilt>
				<img src="new_login/images/img-01.png" alt="IMG">
			</div>

			<form class="login100-form validate-form">
					<span class="login100-form-title">
						Member Login
					</span>

				<div class="container-login100-form-btn">
					<a href="auth/facebook" class="login100-form-btn">
						<i class="fa fa-facebook mr-2"></i> Login
					</a>
				</div>

			</form>
		</div>
	</div>
</div>




<!--===============================================================================================-->
<script src="new_login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
<script src="new_login/vendor/tilt/tilt.jquery.min.js"></script>
<script >
	$('.js-tilt').tilt({
		scale: 1.1
	})
</script>
<!--===============================================================================================-->


</body>
</html>
