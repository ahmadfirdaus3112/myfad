/*
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

import Vue from 'vue'
import VueRouter from "vue-router";

Vue.use(VueRouter);

function loadView(view) {
    return () => import( /* webpackChunkName: "view-[request]" */ `./components/${view}.vue`)
}

let routes = [{
        path: '/dashboard',
        name: 'dashboard',
        component: loadView('Dashboard')
    },
    {
        path: '/',
        component: require('./components/Dashboard.vue').default
    },
    /*{path: '/dashboard', component: require('./components/Dashboard.vue')},*/
    /*{ path: '/developer', component: require('./components/Developer.vue').default },*/
    {
        path: '/users',
        component: require('./components/Users.vue')
    },
    {
        path: '/profile',
        component: require('./components/Profile.vue')
    },
    /*404 pages*/
    {
        path: '*',
        component: () => import('./components/NotFound.vue')
    },
    /*Subscriber route*/
    {
        path: '/subscriber_profile',
        component: () => import('./components/subscriber/profile/SubscriberProfile.vue')
    },
    {
        path: '/subscriber_earning',
        component: require('./components/subscriber/earning/SubscriberEarning.vue').default
    },
    {
        path: '/subscriber_cashout',
        component: require('./components/subscriber/cashout/SubscriberCashout.vue').default
    },
    {
        path: '/subscriber_network',
        component: require('./components/subscriber/network/SubscriberNetwork.vue').default
    },
    {
        path: '/subscriber_referral',
        component: require('./components/subscriber/referral/SubscriberReferral.vue').default
    },
    {
        path: '/subscriber_ads',
        component: require('./components/subscriber/advertisement/Home.vue').default
    },
    /*merchant*/
    {
        path: '/merchant_register',
        component: require('./components/merchant/register/MerchantRegister.vue').default
    },
    {
        path: '/merchant_admin',
        component: require('./components/merchant/administrator/MerchantAdministrator.vue').default
    },
    {
        path: '/merchant_profile',
        component: require('./components/merchant/profile/MerchantProfile.vue').default
    },
    {
        path: '/merchant_page',
        component: require('./components/merchant/page/PageManagement.vue').default
    },
    {
        path: '/merchant_ads',
        component: require('./components/merchant/ads/AdsManagement.vue').default
    },
    {
        path: '/merchant_contest',
        component: require('./components/merchant/contest/ContestManagement.vue').default
    },
    {
        path: '/merchant_token',
        component: require('./components/merchant/token/MerchantToken.vue').default
    },
    /*admin*/
    {
        path: '/reviewer_inbox',
        component: require('./components/reviewer/ReviewerInbox.vue').default
    },
    {
        path: '/reviewer_new_ads',
        component: require('./components/reviewer/ReviewerNewAds.vue').default
    },
    {
        path: '/reviewer_verified_ads',
        component: require('./components/reviewer/ReviewerVerifiedAds.vue').default
    },
];

export default new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
});
