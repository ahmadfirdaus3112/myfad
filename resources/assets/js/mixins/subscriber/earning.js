export const earning = {
  data() {
    return {
		earning:"",
    };
  },
  created: function() {
  this.getEarning()
  },
  methods: {
    getEarning() {
		axios
			.get("api/subscriber/earning")
			.then(response => (this.earning = response.data))
			.catch(error => console.log(error.response.data));
	},
  }
};
