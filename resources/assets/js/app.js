/*
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

import Vue from 'vue';
import router from './routes';
//gate(ACL)
import Gate from './Gate';
//language
import VueI18n from 'vue-i18n';
import Locales from './vue-i18n-locales.generated.js';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/*import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import enLocale from 'element-ui/lib/locale/lang/en';

Vue.use(ElementUI, {enLocale})*/

window.Vue = require('vue');

Vue.prototype.$gate = new Gate(window.user);

//pagination
Vue.component('pagination', require('laravel-vue-pagination'));

//fb
/*const vue_fb = {};
vue_fb.install = function install(Vue, options) {
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0]
        if (d.getElementById(id)) {return}
        js = d.createElement(s)
        js.id = id
        js.src = "//connect.facebook.net/en_US/sdk.js"
        fjs.parentNode.insertBefore(js, fjs)
        console.log('setting fb sdk')
    }(document, 'script', 'facebook-jssdk'))

    window.fbAsyncInit = function onSDKInit() {
        FB.init(options);
        FB.AppEvents.logPageView();
        FB.XFBML.parse();
        Vue.FB = FB;
        window.dispatchEvent(new Event('fb-sdk-ready'))
    };
    Vue.FB = undefined
};

Vue.use(vue_fb, {
    appId: '1700950133453002',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v3.0'
});*/
Vue.use(VueI18n);

const i18n = new VueI18n({
	locale: 'my',
	fallbackLocale: 'my',
	messages: Locales
});

Vue.prototype.$locale = {
	change(lang) {
		i18n.locale = lang;
	},
	current() {
		return i18n.locale;
	}
};
//moment js
Vue.use(require('vue-moment'));

//vee-validate
import VeeValidate from 'vee-validate';
//import validationMessages from 'vee-validate/dist/locale/en';
Vue.use(VeeValidate, {
	aria: false,
	classes: true,
	classNames: {
		valid: 'is-valid',
		invalid: 'is-invalid'
	},
	i18nRootKey: 'validation', // customize the root path for validation messages.
	i18n,
	dictionary: {
		en: Locales.en,
		my: Locales.my
	}
});
//
window.Fire = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('passport-clients', require('./components/passport/Clients.vue').default);

Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);

Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default);

/*Vue.component("not-found", require("./components/NotFound.vue").default);*/

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('vue-topmenu', require('./components/layout/TopMenu.vue').default);
Vue.component('vue-sidebar', require('./components/layout/Sidebar.vue').default);
Vue.component('vue-footer', require('./components/layout/Footer.vue').default);

const app = new Vue({
	i18n,
	el: '#app',
	router
});
