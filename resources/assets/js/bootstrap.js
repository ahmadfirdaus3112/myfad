/*
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

window._ = require("lodash");
window.Popper = require("popper.js").default;

import Vue from "vue";

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);
//modal
import VModal from 'vue-js-modal'
//sweet alert
import swal from "sweetalert2";
//vue-progressbar
import VueProgressBar from "vue-progressbar";
//form alert
import {AlertError, Form, HasError} from "vform";
//moment
import moment from "moment";
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
/* import Table from 'buefy/dist/components/table'
import 'buefy/dist/buefy.css'
Vue.use(Table) */


try {
	window.$ = window.jQuery = require("jquery");
	require("bootstrap");
	require("admin-lte");
} catch (e) {
}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
	window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
	console.error(
		"CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token"
	);
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

Vue.use(VModal);

window.swal = swal;

//toast
const toast = swal.mixin({
	toast: true,
	position: "top-end",
	showConfirmButton: false,
	timer: 3000
});
window.toast = toast;

Vue.use(VueProgressBar, {
	color: "rgb(148, 0, 211)",
	failedColor: "red",
	height: "5px"
});

window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

Vue.filter("upText", function (text) {
	return text.charAt(0).toUpperCase() + text.slice(1);
});
Vue.filter("myDate", function (created) {
	return moment(created).format("MMMM Do YYYY");
});
