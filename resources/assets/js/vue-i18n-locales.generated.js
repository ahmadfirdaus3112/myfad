export default {
    "en": {
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "buttons": {
            "add": "Add",
            "approve": "Approve",
            "cancel": "Cancel",
            "change_file": "Change file",
            "choose_file": "Choose file",
            "delete": "Delete",
            "edit": "Edit",
            "ok": "OK",
            "open": "Open",
            "publish": "Publish",
            "reject": "Reject",
            "save": "Save",
            "sending": "Sending",
            "update": "Update"
        },
        "errors": {
            "fatal_error": "A fatal error occurred, please contact us.",
            "generic_error": "An error occurred, please try again.",
            "unauthorized": "Unauthorized.",
            "forbidden": "Forbidden."
        },
        "front": {
            "deleted_successfully": "It has been deleted successfully.",
            "password_changed_successfully": "Your password has been changed successfully.",
            "passwords_not_match": "The password confirmation does not match.",
            "settings": "Settings",
            "delete_user_confirmation": "Are you sure you want want to delete this user?",
            "language": "English"
        },
        "login": {
            "confirm_password": "Confirm password",
            "description": "A Laravel 5.5 Single Page Application boilerplate using Vue.js 2.5, Bootstrap 4, TypeScript, Sass and Pug.",
            "forgot_password": "Forgot Your Password?",
            "login": "Login",
            "keep_connected": "Keep me connected",
            "register": "Register",
            "reset_password": "Reset password",
            "send_reset_link": "Send the reset link",
            "account_created": "Your account has been created successfully."
        },
        "messages": {
            "dashboard": "Dashboard",
            "user": "User",
            "management": "Management",
            "profile": "Profile",
            "logout": "Logout",
            "success": "Success",
            "updated": "Updated!"
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "settings": {
            "new_password": "New password",
            "password_confirmation": "Password confirmation"
        },
        "sidebar": {
            "dashboard": "Dashboard",
            "manage": "MANAGE",
            "welcome": "Welcome, {name}"
        },
        "strings": {
            "admin": "Administrator",
            "email": "E-mail",
            "image": "Image",
            "loading": "Loading",
            "logo": "Logo",
            "name": "Name",
            "normal": "Normal",
            "optional": "Optional",
            "password": "Password",
            "required": "Required",
            "text": "Text",
            "title": "Title",
            "user": "User",
            "welcome": "Welcome",
            "example": "Example",
            "home": "Home",
            "messages": "Messages",
            "users": "Users",
            "settings": "Settings",
            "clicked": "You've clicked on example 1",
            "example2": "Example 2",
            "clicked2": "You've clicked on example 2",
            "private_channel": "Private Channel",
            "new_private_message": "New Private Message",
            "public_channel": "Public Channel",
            "new_public_message": "New Public Message"
        },
        "users": {
            "add_user": "Add user",
            "edit_user": "Edit user",
            "no_users": "There are no registered users.",
            "user_type": "User type",
            "user_lang": "English"
        },
        "validation": {
            "message": "The given data was invalid.",
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "messages": {
                "_default": "This field is required.",
                "alpha": "Only alphabet",
                "required": "This field is required.",
                "max": "This field cannot over 5 number.",
                "length": "This field cannot less."
            },
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                },
                "sbr_bank_ic": {
                    "length": "Only 12 numbers."
                },
                "postcode": {
                    "digits": "Only 5 numbers."
                }
            },
            "attributes": {
                "email": "e-mail",
                "password": "password",
                "type_id": "user type",
                "null": "empty"
            }
        }
    },
    "my": {
        "auth": {
            "failed": "Las credenciales no se han encontrado.",
            "general_error": "No tiene suficientes permisos..",
            "password_used": "No puede establecer una contraseña que haya utilizado previamente.",
            "socialite": {
                "unacceptable": "{provider} no es un tipo de autenticación válida."
            },
            "throttle": "Demasiados intentos de inicio de sesión. Vuelva a intentarlo en {seconds} segundos.",
            "unknown": "Se ha producido un error desconocido."
        },
        "buttons": {
            "add": "Añadir",
            "approve": "Aprobar",
            "cancel": "Cancelar",
            "change_file": "Cambiar archivo",
            "choose_file": "Elija el archivo",
            "delete": "Borrar",
            "edit": "Editar",
            "ok": "OK",
            "open": "Abierto",
            "publish": "Publicar",
            "reject": "Rechazar",
            "save": "Salvar",
            "sending": "Enviando",
            "update": "Actualizar"
        },
        "errors": {
            "fatal_error": "Se produjo un error fatal, póngase en contacto con nosotros.",
            "generic_error": "Ha ocurrido un error. Por favor intente de nuevo.",
            "unauthorized": "No autorizado."
        },
        "front": {
            "deleted_successfully": "Ha sido eliminado con éxito.",
            "password_changed_successfully": "Tu contraseña ha sido cambiada exitosamente.",
            "passwords_not_match": "La confirmación de contraseña no coincide.",
            "settings": "Configuraciones",
            "delete_user_confirmation": "¿Seguro que quieres eliminar a este usuario?",
            "language": "Español"
        },
        "login": {
            "confirm_password": "Confirmar contraseña",
            "description": "Una plantilla repetitiva de Laravel 5.5 Single Page Application utilizando Vue.js 2.5, Bootstrap 4, TypeScript, Sass y Pug.",
            "forgot_password": "¿Olvidaste tu contraseña?",
            "login": "Iniciar sesión",
            "keep_connected": "Mantenme conectado",
            "register": "Registro",
            "reset_password": "Restablecer la contraseña",
            "send_reset_link": "Enviar el enlace de reinicio",
            "account_created": "Su cuenta ha sido creada con éxito."
        },
        "messages": {
            "dashboard": "Papan Pemuka",
            "user": "Pengguna",
            "management": "Pengurusan",
            "profile": "Profil",
            "logout": "Log Keluar"
        },
        "pagination": {
            "previous": "&laquo; Anterior",
            "next": "Siguiente &raquo;"
        },
        "passwords": {
            "password": "La contraseña debe tener al menos seis caracteres y coincidir con la de su confirmación.",
            "reset": "Su contraseña se ha reiniciado!",
            "sent": "Le hemos enviado el enlace para el reinicio de la contraseña!",
            "token": "El código del reinicio de la contraseña es incorrecto.",
            "user": "El Usuario con este Correo no se ha encontrado."
        },
        "settings": {
            "new_password": "Nueva contraseña",
            "password_confirmation": "confirmación de contraseña"
        },
        "sidebar": {
            "dashboard": "Papan Pemuka",
            "manage": "GESTIONAR",
            "welcome": "Bienvenido, {name}"
        },
        "strings": {
            "admin": "Administrador",
            "email": "E-mail",
            "image": "Imagen",
            "loading": "Cargando",
            "logo": "Logo",
            "name": "Nombre",
            "normal": "Normal",
            "optional": "Opcional",
            "password": "Contraseña",
            "required": "Necesario",
            "text": "Texto",
            "title": "Título",
            "user": "Usuario",
            "welcome": "Bienvenido",
            "example": "Ejemplo",
            "home": "Home",
            "messages": "Mensajes",
            "users": "Usuarios",
            "settings": "Configuraciones",
            "clicked": "Ha hecho clic en el ejemplo 1",
            "example2": "Ejemplo 2",
            "clicked2": "Ha hecho clic en el ejemplo 2",
            "private_channel": "Canal privado",
            "new_private_message": "Nuevo mensaje privado",
            "public_channel": "Canal público",
            "new_public_message": "Nuevo mensaje público"
        },
        "users": {
            "add_user": "Agregar usuario",
            "edit_user": "Editar usuario",
            "no_users": "No hay usuarios registrados",
            "user_type": "Tipo de usuario",
            "user_lang": "Malay"
        },
        "validation": {
            "accepted": "El campo {attribute} debe ser aceptado.",
            "active_url": "El campo {attribute} no es una URL válida.",
            "after": "El campo {attribute} debe ser una fecha posterior a {date}.",
            "after_or_equal": "El campo {attribute} debe ser una fecha posterior o igual a {date}.",
            "alpha": "El campo {attribute} sólo debe contener letras.",
            "alpha_dash": "El campo {attribute} sólo debe contener letras, números y barras.",
            "alpha_num": "El campo {attribute} sólo debe contener letras y números.",
            "array": "El campo {attribute} debe ser una lista.",
            "before": "El campo {attribute} debe ser una fecha anterior a {date}.",
            "before_or_equal": "El campo {attribute} debe ser una fecha anterior o igual a {date}.",
            "between": {
                "numeric": "El campo {attribute} debe ser un número entre {min} y {max}.",
                "file": "El campo {attribute} debe ser entre {min} y {max} kilobytes.",
                "string": "El campo {attribute} debe ser entre {min} y {max} caracteres.",
                "array": "El campo {attribute} debe contener entre {min} y {max} elementos."
            },
            "boolean": "El campo {attribute} debe ser booleano.",
            "confirmed": "El campo {attribute} de confirmación no coincide.",
            "date": "El campo {attribute} no es una fecha válida.",
            "date_format": "La fecha {attribute} debe coincidir con el formato {format}.",
            "different": "El campo {attribute} y {other} deben ser diferentes.",
            "digits": "El número {attribute} debe tener {digits} dígitos.",
            "digits_between": "El número {attribute} debe tener entre {min} y {max} dígitos.",
            "dimensions": "El {attribute} contiene dimensiones de imagen invalidas.",
            "distinct": "El campo {attribute} contiene un valor duplicado.",
            "email": "El campo {attribute} debe contener un e-mail válido.",
            "exists": "El campo {attribute} seleccionado es inválido.",
            "file": "El {attribute} debe ser un archivo.",
            "filled": "El campo {attribute} es obligatorio.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "El campo {attribute} debe contener una imagen.",
            "in": "El {attribute} seleccionado no es válido.",
            "in_array": "El campo {attribute} no existen en {other}.",
            "integer": "El campo {attribute} debe ser un número entero.",
            "ip": "El campo {attribute} debe contener una dirección IP válida.",
            "ipv4": "El campo {attribute} debe contener una Dirección IPv4 válida.",
            "ipv6": "El campo {attribute} debe contener una Dirección IPv6 válida.",
            "json": "El campo {attribute} debe contener un JSON válido.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "El número {attribute} no debe ser mayor que {max}.",
                "file": "El fichero {attribute} no debe ser mayor que {max} kilobytes.",
                "string": "El texto {attribute} debe tener menos de {max} caracteres.",
                "array": "La lista {attribute} debe contener menos de {max} elemnetos."
            },
            "mimes": "El fichero {attribute} debe tener el formato/s {values}.",
            "min": {
                "numeric": "El número {attribute} debe ser mayor o igual a {min}.",
                "file": "El fichero {attribute} debe ser mayor o igual a {min} kilobytes.",
                "string": "El texto {attribute} debe tener, al menos, {min} caracteres.",
                "array": "La lista {attribute} debe contener, al menos {min} elemnetos."
            },
            "not_in": "El {attribute} seleccionado no es válido.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "El campo {attribute} debe ser un número.",
            "present": "El campo {attribute} debe estar presente.",
            "regex": "El formato de {attribute} no es válido.",
            "required": "El campo {attribute} es obligatorio.",
            "required_if": "El campo {attribute} es obligatorio cuando {other} tiene valor {value}.",
            "required_unless": "El campo {attribute} es obligatorio, excepto cuando {other} esta en {values}.",
            "required_with": "El campo {attribute} es obligatorio cuando {values} están presentes.",
            "required_with_all": "El campo {attribute} es obligatorio cuando {values} están presentes.",
            "required_without": "El campo {attribute} es obligatorio cuando {values} no están presentes.",
            "required_without_all": "El campo {attribute} es obligatorio cuando ninguno de {values} están presentes.",
            "same": "El campo {attribute} y {other} deben coincidir.",
            "size": {
                "numeric": "El número {attribute} debe tener {size} caracteres.",
                "file": "El fichero {attribute} debe tener {size} kilobytes.",
                "string": "El texto {attribute} debe tener {size} caracteres.",
                "array": "La lista {attribute} debe contener {size} elemnetos."
            },
            "string": "El campo {attribute} debe contener texto.",
            "timezone": "El campo {attribute} debe contener una zona horaria válida.",
            "unique": "El campo {attribute} ya está en uso.",
            "uploaded": "El campo {attribute} no se pudo actualizar.",
            "url": "El enlace {attribute} debe tener un formato válido.",
            "messages": {
                "_default": "This field is required.",
                "alpha": "Only alphabet",
                "required": "This field is required.",
                "max": "This field cannot over 5 number.",
                "length": "This field cannot less."
            },
            "custom": {
                "attribute-name": {
                    "rule-name": "mensaje-personalizado"
                }
            },
            "attributes": {
                "email": "e-mail",
                "password": "contraseña",
                "type_id": "tipo usuario"
            }
        }
    }
}
