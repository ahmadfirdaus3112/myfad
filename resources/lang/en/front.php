<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'deleted_successfully' => 'It has been deleted successfully.',
    'password_changed_successfully' => 'Your password has been changed successfully.',
    'passwords_not_match' => 'The password confirmation does not match.',
    'settings' => 'Settings',

    'delete_user_confirmation' => 'Are you sure you want want to delete this user?',
    'language' => 'English',

];
