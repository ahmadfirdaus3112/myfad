<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'add' => 'Add',
    'approve' => 'Approve',
    'cancel' => 'Cancel',
    'change_file' => 'Change file',
    'choose_file' => 'Choose file',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'ok' => 'OK',
    'open' => 'Open',
    'publish' => 'Publish',
    'reject' => 'Reject',
    'save' => 'Save',
    'sending' => 'Sending',
    'update' => 'Update',

];
