<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'add_user' => 'Add user',
    'edit_user' => 'Edit user',
    'no_users' => 'There are no registered users.',
    'user_type' => 'User type',
    'user_lang' => 'English',

];
