<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'new_password' => 'New password',
    'password_confirmation' => 'Password confirmation',

];
