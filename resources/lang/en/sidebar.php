<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'dashboard' => 'Dashboard',
    'manage' => 'MANAGE',
    'welcome' => 'Welcome, :name',

];
