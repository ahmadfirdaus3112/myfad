<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'fatal_error' => 'A fatal error occurred, please contact us.',
    'generic_error' => 'An error occurred, please try again.',
    'unauthorized' => 'Unauthorized.',
    'forbidden' => 'Forbidden.',

];
