<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'add_user' => 'Agregar usuario',
    'edit_user' => 'Editar usuario',
    'no_users' => 'No hay usuarios registrados',
    'user_type' => 'Tipo de usuario',
    'user_lang' => 'Malay',

];
