<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'dashboard' => 'Papan Pemuka',
    'manage' => 'GESTIONAR',
    'welcome' => 'Bienvenido, :name',

];
