<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'fatal_error' => 'Se produjo un error fatal, póngase en contacto con nosotros.',
    'generic_error' => 'Ha ocurrido un error. Por favor intente de nuevo.',
    'unauthorized' => 'No autorizado.',

];
