<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'deleted_successfully' => 'Ha sido eliminado con éxito.',
    'password_changed_successfully' => 'Tu contraseña ha sido cambiada exitosamente.',
    'passwords_not_match' => 'La confirmación de contraseña no coincide.',
    'settings' => 'Configuraciones',

    'delete_user_confirmation' => '¿Seguro que quieres eliminar a este usuario?',
    'language' => 'Español',

];
