<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

return [

    'new_password' => 'Nueva contraseña',
    'password_confirmation' => 'confirmación de contraseña',

];
