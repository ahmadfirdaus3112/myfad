<?php

/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

use Illuminate\Http\Request;

/*php
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();

});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/merchant', 'HomeController@merchant');
    Route::get('/merchant_id', 'HomeController@merchant_id');

    /*Fb Graph Route*/
    Route::get('/userprofile', 'GraphController@retrieveUserProfile');
    Route::get('/fb_post', 'GraphController@getPagePostList');
    Route::get('/fb_test', 'GraphController@test');

    Route::Resources(['user' => 'API\UserController']);
    Route::get('profile', 'API\UserController@profile');
    Route::put('profile', 'API\UserController@updateProfile');

    Route::get('languages', 'API\LanguageController@languages');

    Route::get('fbpost', 'API\AdvertisementController@index');

    /*Subscriber Route*/
    Route::get('subscriber/profile', 'API\SubscriberController@index');
    Route::put('subscriber/profile', 'API\UserController@updateProfile');

    Route::get('subscriber/network', 'API\UserController@profile');

    Route::post('setlocale', 'API\LanguageController@switchLang');
    Route::get('getlocale', 'API\LanguageController@getLang');

    Route::get('findUser', 'API\UserController@search');

    Route::get('subscriber/earning', 'API\SubscriberEarningController@getEarning');
    Route::get('subscriber/earning_history', 'API\SubscriberEarningController@getList');
    Route::get('subscriber/earning_summary', 'API\SubscriberEarningController@getEarningSummary');

    Route::get('subscriber/cashout_history', 'API\CashoutController@getSubscriberCashoutHistory');
    Route::get('subscriber/cashout_details', 'API\CashoutController@getSubscriberCashoutDetails');
    Route::post('subscriber/post_cashout', 'API\CashoutController@postSubscriberCashoutDetails');
    Route::get('subscriber/test', 'API\CashoutController@test2');
    Route::get('subscriber/test3', 'API\CashoutController@test3');

    Route::get('subscriber/get_city/{id}', 'API\CashoutController@getCity');
    Route::get('subscriber/subscriber_network', 'API\NetworkController@getNetwork');
    Route::get('subscriber/subscriber_message', 'API\SubscriberController@index');
    Route::put('subscriber/subscriber_message_update', 'API\NetworkController@updateMsg');
    Route::get('subscriber/lvl_1', 'API\NetworkController@getNetworkLevel1');
    Route::get('subscriber/lvl_2', 'API\NetworkController@getNetworkLevel2');
    Route::get('subscriber/network_merchant', 'API\NetworkController@getNetworkMerchant');

    Route::get('subscriber/share_count', 'API\ShareController@getPendingShares');
    Route::get('subscriber/selected_ads', 'API\ShareController@getSelectedShare');
    Route::get('subscriber/charity_ads', 'API\ShareController@getCharityShare');
    Route::get('subscriber/open_ads', 'API\ShareController@getOpenShare');
    Route::get('subscriber/contest_ads', 'API\ShareController@getContestShare');

    /*Merchant Route*/
    Route::get('merchant_profile/{mct_id}', 'API\MerchantController@index');
    Route::put('merchant_profile/{id}', 'API\MerchantController@update');

    //Route::Resources(['merchant_page.id' => 'API\MerchantPageController']);
    Route::get('merchant_page/{mct_id}', 'API\MerchantPageController@index');
    Route::post('merchant_page/{mct_id}', 'API\MerchantPageController@store');
    Route::put('merchant_page/{id}', 'API\MerchantPageController@update');
    Route::delete('merchant_page/{id}', 'API\MerchantPageController@destroy');

    Route::get('merchant_ads/{mct_id}', 'API\AdvertisementController@index');
    Route::post('merchant_ads/{mct_id}', 'API\AdvertisementController@store');
    Route::put('merchant_ads/{id}', 'API\AdvertisementController@update');
    Route::delete('merchant_ads/{id}', 'API\AdvertisementController@destroy');

    Route::get('merchant_contest/{mct_id}', 'API\ContestAdvertisementController@index');
    Route::post('merchant_contest/{mct_id}', 'API\ContestAdvertisementController@store');
    Route::put('merchant_contest/{id}', 'API\ContestAdvertisementController@update');
    Route::delete('merchant_contest/{id}', 'API\ContestAdvertisementController@destroy');

    //Route::Resources(['merchant_ads' => 'API\AdvertisementController']);
    //Route::Resources(['merchant_contest' => 'API\ContestAdvertisementController']);

    Route::get('merchant_admin/', 'API\MerchantAdministratorController@index');
    Route::post('merchant_admin/new_admin/{id}', 'API\MerchantAdministratorController@store');
    Route::delete('merchant_admin/delete_admin/{id}', 'API\MerchantAdministratorController@delete');

    Route::get('token_purchase/{mct_id}', 'API\MerchantPaymentController@getTokenPurchaseHistory');
    Route::get('token_count/{mct_id}', 'API\MerchantController@getTokenCount');

    Route::get('credits/{mct_id}', 'API\TokenController@getCredits');

    Route::get('pages', 'API\MerchantPageController@index2');

    Route::get('states', 'API\MerchantController@states');
    Route::get('ages', 'API\MerchantController@ages');
    Route::get('genders', 'API\MerchantController@genders');
    Route::get('races', 'API\MerchantController@races');
    Route::get('religions', 'API\MerchantController@religions');
    Route::get('educations', 'API\MerchantController@educations');
    Route::get('occupations', 'API\MerchantController@occupations');
    Route::get('incomes', 'API\MerchantController@incomes');
    Route::get('interests', 'API\MerchantController@interests');

    /*Reviewer*/
    Route::get('reviewer_inbox', 'API\ReviewerController@inbox');
    Route::get('reviewer_new_ads', 'API\ReviewerController@newAds');
    Route::put('reviewer_lock_ads', 'API\ReviewerController@lockTransaction');
    Route::put('reviewer_release_ads', 'API\ReviewerController@releaseTransaction');

    Route::put('reviewer_approve_ads', 'API\ReviewerController@approveAds');
    Route::put('reviewer_reject_ads', 'API\ReviewerController@rejectAds');

    Route::get('reviewer_verified_ads', 'API\ReviewerController@verifiedAds');

});
