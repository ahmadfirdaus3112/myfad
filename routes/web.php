<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

/* Route::get('/', function () {
return view('auth.login');
}); */

Auth::routes();
Route::get('/impersonate/{id}', 'Auth\LoginController@impersonate');
Route::get('/home', 'HomeController@index')->name('home');

/*Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');*/

Route::get('auth/facebook', 'Auth\LoginController@redirectToFacebookProvider');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderFacebookCallback');

Route::get('{path}', "HomeController@index")->where('path', '([A-z\d-\/_.]+)?');

Route::group(['middleware' => [
    'auth',
]], function () {
    Route::get('/userprofile', 'GraphController@retrieveUserProfile');

    Route::post('/user', 'GraphController@publishToProfile');

    Route::get('/', function () {
        return view('home');
    });
});
