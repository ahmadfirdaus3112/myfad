<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'state_id';
    protected $table = 'state';
}
