<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'el_id';
    protected $table = 'education_level';
}
