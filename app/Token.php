<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    //
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'credit_id';
    protected $table = 'credits';
}
