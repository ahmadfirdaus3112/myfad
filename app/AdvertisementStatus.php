<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementStatus extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'adstatus_id';
    protected $table = 'advert_status';
}
