<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'occ_id';
    protected $table = 'occupation';
}
