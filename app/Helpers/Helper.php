<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Helpers;

use App\Merchant;
use App\MerchantAdmin;
use App\Subscriber;
use App\Token;
use App\User;

class Helper
{

    public static function SayHello()
    {
        return "SayHello";
    }

    public static function merchant_admin()
    {
        //$user_lang = auth()->user()->user_lang;
        //$user_merchant = auth()->user()->user_merchant_id;
        //dump($user_lang);
        $merchant_ori = User::where('user_id', auth()->user()->user_id)->pluck('user_merchant_id');
        $merchant_new = MerchantAdmin::where('ma_user_id', auth()->user()->user_id)->pluck('ma_merchant_id');

        $collection = collect($merchant_ori);

        $merged = $collection->merge($merchant_new)->unique()->toArray();

        $merchant = Merchant::whereIn('mct_id', $merged)->get();

        return $merged;
    }
    public function getCredits($mct_id)
    {
        //
        return Token::where('credit_merchant',$mct_id)->where('credit_used',0)->where('credit_ads_id',null)->count();

    }
    public function getEarning($sbr_id)
    {
        //
        return Subscriber::where('sbr_id',$sbr_id);

    }
}
