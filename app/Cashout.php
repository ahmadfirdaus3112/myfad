<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashout extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'co_id';
    protected $table = 'cashout';
}
