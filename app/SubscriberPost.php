<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberPost extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'spt_id';
    protected $table = 'subscriber_post';
}
