<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    //
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'mct_id';
    protected $table = 'merchant';

    public function merchant_admin()
    {
        return $this->belongsTo(MerchantAdmin::class, 'mct_id', 'ma_merchant_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'mct_id', 'user_merchant_id');
    }
}
