<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'slr_id';
    protected $table = 'salary';
}
