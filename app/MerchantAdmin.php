<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantAdmin extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'ma_id';
    protected $table = 'merchant_admin';

    public function user()
    {
        return $this->belongsTo(User::class, 'ma_user_id', 'user_id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'ma_merchant_id', 'mct_id');
    }
}
