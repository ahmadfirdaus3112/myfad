<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Subscriber extends Authenticatable
{
    use Notifiable, HasApiTokens;

    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'sbr_id';
    protected $table = 'subscriber';
}
