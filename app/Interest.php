<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'it_id';
    protected $table = 'interest';
}
