<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPage extends Model
{
    //
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = 'mp_id';
    protected $table = 'merchant_page';
}
