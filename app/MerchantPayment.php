<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPayment extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'mp_refNo';
    protected $table = 'merchant_payment';
}
