<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    //
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'ads_id';
    protected $table = 'advertisement';

    public function status()
    {
        return $this->belongsTo(AdvertisementStatus::class, 'ads_status', 'adstatus_id');
    }
    public function credits()
    {
        return $this->hasMany(Token::class, 'credit_merchant', 'ads_merchant')->where('credit_used',0)->where('credit_ads_id',null);
    }

    public function share_progress()
    {
        return $this->hasMany(SubscriberPost::class, 'spt_adsId', 'ads_id')->whereNotNull('spt_fbPostId');
        //return $this->belongsTo(SubscriberPost::class, 'ads_id', 'spt_adsId');
    }

    public function share_progress_contest()
    {
        return $this->hasMany(SubscriberPostContest::class, 'spt_adsId', 'ads_id');
        //return $this->belongsTo(SubscriberPost::class, 'ads_id', 'spt_adsId');
    }
}
