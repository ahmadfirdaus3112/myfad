<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'bank_id';
    protected $table = 'bank';
}
