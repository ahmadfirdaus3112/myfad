<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Auth;

class Language
{
    public function handle($request, Closure $next)
    {

        //$user_lang = $user->language;
        //dd($user);
        if (Session::has('locale') AND array_key_exists(Session::get('locale'), Config::get('app.languages'))) {
            App::setLocale(Session::get('locale'));
        } else {
            App::setLocale(Config::get('app.fallback_locale'));
        }
        return $next($request);
    }
}
