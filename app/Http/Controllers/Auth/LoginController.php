<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Subscriber;
use App\User;
use Auth;
use Facebook\Facebook;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $username = 'user_username';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'user_username';
    }

    public function redirectToFacebookProvider()
    {

        return Socialite::driver('facebook')->scopes([
            "manage_pages", "publish_pages"])->redirect();
    }

    public function handleProviderFacebookCallback(Facebook $fb)
    {
        $auth_user = Socialite::driver('facebook')->user();
        //dump($auth_user);
        //get friends count
        $fb->setDefaultAccessToken($auth_user->token);

        try {
            // Returns a `FacebookFacebookResponse` object
            $endpoint = "me/friends"; //Not Exist
            $response = $fb->get($endpoint);

            $Edge = $response->getGraphEdge();
            $totalCount = $Edge->getTotalCount();

            //dd($totalCount);

        } catch (FacebookExceptionsFacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookExceptionsFacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        /*end*/

        /*$user = User::updateOrCreate(
            [
                'email' => $auth_user->email
            ],
            [
                'token' => $auth_user->token,
                'name' => $auth_user->name,
                'provider_id' => $auth_user->id
            ]
        );*/
        $isEmailExists = User::where('user_username', $auth_user->email)->count();


        if ($isEmailExists) {
            $authUser = User::where('user_username', $auth_user->email)->first();
            $authSubscriber = Subscriber::where('sbr_id', $authUser->user_subscriber_id)->first();
            //dd($auth_user);
            Auth::login($authUser, true);

            //return $authUser;
            //return redirect()->route('home');
            $authSubscriber->update([
                'sbr_fb_token' => $auth_user->token,
            ]);
            return redirect()->to('/dashboard');
        } else {
            $data = array(
                'email' => $auth_user->email,
                'token' => $auth_user->token,
                'name' => $auth_user->name,
                'friends_count' => $totalCount,
            );

            \Session::put($data);

            //Auth::login($user, true);
            return redirect('register')->with($data);
        }


        //return redirect()->to('/register')->with($xx);// Redirect to a secure page
    }

    public function impersonate($id)
    {
        //session(['player_id' => (string)$id]);

        $user = User::where('user_subscriber_id', $id)->first();

        if (Auth::loginUsingId($user->user_id, true)) {
            //Auth::login($user, true);
            return redirect()->to('/dashboard');
        } else {
            return redirect()->back()->with('alert', 'Password dan ID Anda tiada dalam rekod !');
        }
        //return redirect()->showLoginForm();
    }
}
