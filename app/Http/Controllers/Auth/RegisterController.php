<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Subscriber;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $username = 'user_username';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function username()
    {
        return 'user_username';
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:user,user_username',
            /*'email' => 'required|string|max:255|unique:user',*/
            'password' => 'required|string|min:6|confirmed',
            'friends' => 'required|int|min:50',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        //$this->createSubscriber($data);
        $last_id = $this->createSubscriber($data);

        /*$flight = User::updateOrCreate(
            ['email' => Session::get('email')],
            ['price' => 99]
        );*/
        /*$user = User::where('email', Session::get('email'))
            ->update([
                'password' => Hash::make($data['password']),
                'user_username' => $data['email'],
                'user_pwd' => Hash::make($data['password']),
                'user_subscriber_id' => $last_id,
            ]);
        return $user;*/


        return User::updateOrCreate(
            [
                'user_username' => $data['email']
            ],
            [
                /*'name' => $data['name'],*/
                /*'email' => $data['email'],*/
                /*'password' => Hash::make($data['password']),*/

                'user_username' => $data['email'],
                'user_pwd' => Hash::make($data['password']),
                'user_subscriber_id' => $last_id,
                'user_fb_id' => $data['id'],
                /*'token' => $data['token'],*/

            ]);


    }

    protected function createSubscriber(array $data)
    {
        $subscriber = Subscriber::create([
            'sbr_name' => $data['name'],
            'sbr_email' => $data['email'],
            'sbr_pwd' => Hash::make($data['password']),
            'sbr_fb_token' => $data['token'],
        ]);

        //$lastInsertedId = $subscriber->sbr_id;
        //dd($lastInsertedId);
        return  $subscriber->sbr_id;


    }

}
