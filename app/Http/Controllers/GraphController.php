<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class GraphController extends Controller
{
    private $api;

    public function __construct(Facebook $fb)
    {
        $this->middleware(function ($request, $next) use ($fb) {
            $fb->setDefaultAccessToken(Auth::user()->subscriber->sbr_fb_token);
            $this->api = $fb;
            return $next($request);
        });
    }

    public function retrieveUserProfile()
    {

        try {
            $access_token = auth()->user()->subscriber->sbr_fb_token;

            $params = "id,name,link,email,gender,picture,first_name,last_name,cover";
            $user = $this->api->get('/me?fields=' . $params, $access_token)->getGraphNode();

            //dump($user);
            //dd($user);
            return $user;

        } catch (FacebookSDKException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        }

    }

    public function getFriendsCount()
    {
        try {
            // Returns a `FacebookFacebookResponse` object
            $endpoint = "me/friends"; //Not Exist
            $response = $this->api->get($endpoint);

            $likesEdge = $response->getGraphEdge();
            $totalCount = $likesEdge->getTotalCount();

            return $totalCount;
            //dd($totalCount);

        } catch (FacebookExceptionsFacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookExceptionsFacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }


    }

    public function publishToProfile()
    {
        $body = 'Reading JS SDK documentation';

        try {
            $response = $this->api->post('/me/feed', [
                'message' => $body
            ])->getGraphNode()->asArray();
            if ($response['id']) {
                // post created
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }

    public function getPageAccessToken($page_id)
    {
        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $this->api->get('/me/accounts', Auth::user()->token);
        } catch (FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        try {
            $pages = $response->getGraphEdge()->asArray();
            foreach ($pages as $key) {
                if ($key['id'] == $page_id) {
                    return $key['access_token'];
                }
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }

    public function getPagePostList(Request $request)
    {
        try {
            $page_id = $request['page_id'];
            $access_token = auth()->user()->subscriber->sbr_fb_token;

            $params = "link";
            $userx = $this->api->get($page_id . '/feed?fields=' . $params, $access_token)->getGraphEdge()->asArray();

            //$data = [];
            foreach ($userx as $row) {
                $str = str_replace('_', '/posts/', $row['id']);
                $var = 'https://www.facebook.com/' . $str;
                $data[] = $var;

            }

            //$paginate = new LengthAwarePaginator($data, count($data), 10, 1, ['path' => url('api/fb_post')]);
            return $data;

        } catch (FacebookExceptionsFacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookExceptionsFacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

    }

    public function test(Request $request)
    {
        try {
            // Returns a `FacebookFacebookResponse` object
            $endpoint = "me?fields=link"; //Not Exist
            $response = $this->api->get($endpoint);

            $likesEdge = $response->getGraphNode();
            dd($likesEdge);

            //dd($totalCount);

        } catch (FacebookExceptionsFacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookExceptionsFacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }


    }
}
