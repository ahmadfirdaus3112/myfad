<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers;

use App\Merchant;
use App\MerchantAdmin;
use App\User;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_lang = auth()->user()->user_lang;
        $user_merchant = auth()->user()->user_merchant_id;
        //dump($user_lang);
        $merchant_ori = User::where('user_id', auth()->user()->user_id)->pluck('user_merchant_id');
        $merchant_new = MerchantAdmin::where('ma_user_id', auth()->user()->user_id)->pluck('ma_merchant_id');

        $collection = collect($merchant_ori);

        $merged = $collection->merge($merchant_new)->unique();

        $merchant = Merchant::whereIn('mct_id', $merged)->get();
        //dump($merchant);

        if ($user_lang !== null) {

            \Session::put('locale', $user_lang);
            \App::setLocale($user_lang);
            //return response()->json(\Session::get('locale'));
        }
        //dump($test);
        return view('home', compact('merchant', 'user_merchant'));
    }

    public function merchant_id()
    {
        $user_lang = auth()->user()->user_lang;
        $user_merchant = auth()->user()->user_merchant_id;
        //dump($user_lang);
        $merchant_ori = User::where('user_id', auth()->user()->user_id)->pluck('user_merchant_id');
        $merchant_new = MerchantAdmin::where('ma_user_id', auth()->user()->user_id)->pluck('ma_merchant_id');

        $collection = collect($merchant_ori);

        $merged = $collection->merge($merchant_new)->unique();

        $merchant = Merchant::whereIn('mct_id', $merged)->get();
        //dump($merchant);


        //dump($test);
        return $user_merchant;
    }

    public function merchant()
    {
        $user_lang = auth()->user()->user_lang;
        $user_merchant = auth()->user()->user_merchant_id;
        //dump($user_lang);
        $merchant_ori = User::where('user_id', auth()->user()->user_id)->pluck('user_merchant_id');
        $merchant_new = MerchantAdmin::where('ma_user_id', auth()->user()->user_id)->pluck('ma_merchant_id');

        $collection = collect($merchant_ori);

        $merged = $collection->merge($merchant_new)->unique();

        $merchant = Merchant::whereIn('mct_id', $merged)->get();
        //dump($merchant);


        //dump($test);
        return $merchant;
        //return response()->json($merchant);
    }
}
