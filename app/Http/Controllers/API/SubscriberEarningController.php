<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SubscriberEarningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getEarning()
    {
        //
        $sbr_id = \auth()->user()->subscriber->sbr_earning;

        return $sbr_id;
    }
    public function getList()
    {
        try {
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $tst_typeId = array(1, 2, 3, 5, 6);
            $earnList = DB::table('transaksi')
                ->leftJoin('transaction_type', 'transaction_type.id', '=', 'transaksi.tst_typeId')
                ->leftJoin('advertisement', 'advertisement.ads_id', '=', 'transaksi.tst_adsId')
                ->leftJoin('subscriber_post', 'subscriber_post.spt_id', '=', 'transaksi.tst_sbrpstId')
                ->leftJoin('subscriber', 'subscriber.sbr_id', '=', 'subscriber_post.spt_sbrId')
                ->where('tst_benificialId', $sbr_id)
                ->whereIn('tst_typeId', $tst_typeId)
                ->select('transaksi.*', 'transaksi.id as transaksi_id', 'transaction_type.*', 'advertisement.ads_title', 'advertisement.ads_url', 'subscriber_post.spt_postdate', 'subscriber.sbr_name')
                ->orderBy('transaksi.tst_date', 'DESC')
                ->paginate(5);

            return $earnList;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function getEarningSummary()
    {
        try {
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $tst_typeId = array(1, 2, 3, 5, 6);
            $sumList = DB::table('transaksi')
                ->join('advertisement', 'advertisement.ads_id', '=', 'transaksi.tst_adsId')
                ->where('tst_benificialId', $sbr_id)
                ->whereIn('tst_typeId', $tst_typeId)
                ->select('transaksi.tst_adsId as ads_id', DB::raw("ROUND(SUM(tst_amount), 4) as tst_amount"), 'advertisement.ads_title', 'advertisement.ads_url', DB::raw("(SELECT ROUND(SUM(tst_amount), 4) FROM transaksi WHERE tst_typeId=1 AND `tst_benificialId`=$sbr_id AND tst_adsId=ads_id) AS SUB,(SELECT ROUND(SUM(tst_amount), 4) FROM transaksi WHERE tst_typeId=2 AND `tst_benificialId`=$sbr_id AND tst_adsId=ads_id) AS DOWNLINE1,(SELECT ROUND(SUM(tst_amount), 4) FROM transaksi WHERE tst_typeId=3 AND `tst_benificialId`=$sbr_id AND tst_adsId=ads_id) AS DOWNLINE2"))
                ->groupBy('transaksi.tst_adsId')
                ->orderBy('advertisement.ads_created_date', 'DESC')
                ->paginate(5);

            return $sumList;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}
