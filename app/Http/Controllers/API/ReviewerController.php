<?php

namespace App\Http\Controllers\API;

use App\Advertisement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Gate;

class ReviewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inbox()
    {
        if (Gate::allows('isUser')) {
            abort(403, "Sorry, you are not authorized to access this page :(");
        }
        try {
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $earnList = DB::table('advertisement')
                ->join('advert_status', 'advert_status.adstatus_id', '=', 'advertisement.ads_status')
                ->where('ads_status', 2)
                ->select('advertisement.*', 'advert_status.*', DB::raw('(CASE WHEN ads_type=1 THEN "NORMAL" WHEN ads_type = 3 THEN "CONTEST" WHEN ads_type = 4 THEN "CHARITY / NEWS" ELSE 0 END) AS ads_type'))
                ->orderBy('ads_created_date', 'DESC')
                ->paginate(5);

            return $earnList;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function newAds()
    {
        try {
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $earnList = DB::table('advertisement')
                ->join('advert_status', 'advert_status.adstatus_id', '=', 'advertisement.ads_status')
                ->where('ads_status', 1)
                ->where(function ($query) {
                    $query->where('ads_locked_by', '')
                        ->orWhereNull('ads_locked_by');
                })
                ->select('advertisement.*', 'advert_status.*', DB::raw('(CASE WHEN ads_type=1 THEN "NORMAL" WHEN ads_type = 3 THEN "CONTEST" WHEN ads_type = 4 THEN "CHARITY / NEWS" ELSE 0 END) AS ads_type'))
                ->orderBy('ads_created_date', 'DESC')
                ->paginate(5);

            return $earnList;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function verifiedAds()
    {
        try {
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $earnList = DB::table('advertisement')
                ->join('advert_status', 'advert_status.adstatus_id', '=', 'advertisement.ads_status')
                ->where('ads_verified_by', 14311)
                ->where('ads_status', '>', 1)
                ->select('advertisement.*', 'advert_status.*', DB::raw('(CASE WHEN ads_type=1 THEN "NORMAL" WHEN ads_type = 3 THEN "CONTEST" WHEN ads_type = 4 THEN "CHARITY / NEWS" ELSE 0 END) AS ads_type'))
                ->orderBy('ads_created_date', 'DESC')
                ->paginate(5);

            return $earnList;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function lockTransaction(Request $request)
    {
        $ads_id = $request['ads_id'];
        $sbr_id = \auth()->user()->subscriber->sbr_id;

        $sp = Advertisement::findOrFail($ads_id);

        $sp->ads_locked_date = date('Y-m-d H:i:s');
        $sp->ads_locked_by = $sbr_id;
        $sp->ads_status = 2;

        $sp->update();


    }

    public function releaseTransaction(Request $request)
    {
        try {
            $ads_id = $request['ads_id'];

            $sp = Advertisement::findOrFail($ads_id);

            $sp->ads_locked_date = null;
            $sp->ads_locked_by = null;
            $sp->ads_status = 1;

            $sp->update();

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function approveAds(Request $request)
    {
        $enddate = new \DateTime();
        $enddate->modify('+1 week');

        $ads_status = 3;
        $start_date = date('Y-m-d H:i:s');
        $end_date = $enddate;

        if ($request['ads_type'] == "CONTEST" || $request['ads_type'] == "CHARITY / NEWS") {
            $ads_status = "5";
        }

        try {
            $ads_id = $request['ads_id'];
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $sp = Advertisement::findOrFail($ads_id);

            $sp->ads_locked_date = null;
            $sp->ads_locked_by = null;
            $sp->ads_startdate = $start_date;
            $sp->ads_endtime = $end_date;
            $sp->ads_verified_date = $start_date;
            $sp->ads_verified_by = $sbr_id;
            $sp->ads_status = $ads_status;

            $sp->update();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function rejectAds(Request $request)
    {

        $start_date = date('Y-m-d H:i:s');

        try {
            $ads_id = $request['ads_id'];
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $sp = Advertisement::findOrFail($ads_id);

            $sp->ads_locked_date = null;
            $sp->ads_locked_by = null;
            $sp->ads_reject_reason = $request['ads_reject_reason'];

            $sp->ads_verified_date = $start_date;
            $sp->ads_verified_by = $sbr_id;
            $sp->ads_status = 98;

            $sp->update();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
