<?php

namespace App\Http\Controllers\API;

use App\Advertisement;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ContestAdvertisementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mct_id)
    {
        if (in_array($mct_id, Helper::merchant_admin())) {
            $user = Advertisement::where('ads_merchant', $mct_id)->where('ads_type', 3)->orderBy('ads_created_date', 'DESC')->with('status')->withCount('share_progress_contest')->withCount('credits')->paginate(10);
            return $user;
        } else {
            return (['isValid' => false, 'errors' => 'Not Authorized']);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $mct_id)
    {
        $this->validate($request, [

            'ads_url' => 'required',
            'ads_title' => 'required|string|max:191',
            'ads_keyword' => 'required',


        ]);

        $contest = Advertisement::create([
            'ads_merchant' => $mct_id,
            'ads_created_by' => \auth()->user()->merchant->mct_email,
            'ads_created_date' => Carbon::now(),
            'ads_status' => 1,
            'ads_credit' => $this->getContestCredits(),
            'ads_title' => $request['ads_title'],
            'ads_url' => $request['ads_url'],
            'ads_keyword' => $request['ads_keyword'],
            'ads_type' => 3,

        ]);
        $insertedId = $contest->ads_id;
        //dump($insertedId);
        $this->updateCredits($mct_id,$insertedId);
        //$this->getCredits($mct_id);

        return $contest;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getCredits($mct_id)
    {
        //
        return Token::where('credit_merchant',$mct_id)->where('credit_used',0)->where('credit_ads_id',null)->count();

    }
    public function updateCredits($mct_id,$insertedId)
    {
        //
        //dump($insertedId);
        return Token::where('credit_merchant',$mct_id)->where('credit_used',0)->where('credit_ads_id',null)->limit($this->getContestCredits())->update([
            'credit_ads_id' => $insertedId,
            'credit_modified_date' => Carbon::now()->toDateTimeString(),
            'credit_used' => 1,
        ]);

        //return $this->getCredits($mct_id);
    }
    public function getContestCredits()
    {
        return 140;
    }
}
