<?php

namespace App\Http\Controllers\API;

use App\Token;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TokenController extends Controller
{
    //
    public function getCredits($mct_id)
    {
        //
        return Token::where('credit_merchant',$mct_id)->where('credit_used',0)->where('credit_ads_id',null)->count();

    }
}
