<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers\API;

use App\Bank;
use App\Cashout;
use App\City;
use App\State;
use App\Subscriber;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use mysql_xdevapi\Exception;

class CashoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSubscriberCashoutDetails()
    {
        $bank_list = Bank::pluck('bank_name', 'bank_id');
        $state_list = State::pluck('state_name', 'state_id');


        $qs = "SELECT * from subscriber
		WHERE sbr_id=:sbr_id";
        $rows = DB::select(DB::raw($qs), ['sbr_id' => Auth::user()->subscriber->sbr_id]);

        //$rows = R::getAll ( $qs,[$subscriber['sbr_id']] );
        $sbr_earning = Auth::user()->subscriber->sbr_earning;

        $data['sbr_earning'] = $sbr_earning;

        foreach ($rows as $row) {
            $data['sbr_id'] = $row->sbr_id;
            $data['sbr_bank'] = $row->sbr_bankId;
            $data['sbr_bank_accholder'] = $row->sbr_bank_acc_holder;
            $data['sbr_bank_no'] = $row->sbr_bank_accno;
            $data['sbr_bank_ic'] = $row->sbr_bank_accicno;
            $data['sbr_postcode'] = $row->sbr_zipcode;

            $data['sbr_city'] = $row->sbr_city;
            $data['sbr_state'] = $row->sbr_state;
        }
        $data['bank_list'] = $bank_list;
        $data['state_list'] = $state_list;

        $city_list = City::where('city_state', $data['sbr_state'])->pluck('city_name', 'city_id');
        $data['city_list'] = $city_list;

        $result = $data;
        //dd($result);
        print json_encode($result);
        //dd($result);
        //print view('menu.cashout-modal', compact('banks', 'bank_name', 'email', 'phone','bank_accno'));
        exit();

    }

    public function getCity($id)
    {
        return City::where('city_state', $id)->pluck('city_name', 'city_id');
    }


    public function getSubscriberCashoutHistory()
    {

        /*$qs = "SELECT * FROM cashout INNER JOIN cashout_status  AS cashout_status ON (cashout_status.cos_id=cashout.co_status) WHERE co_bef_id=:sbr_id AND co_type=1 ORDER BY co_date_created DESC LIMIT 0,50";
        $rows = DB::select(DB::raw($qs), ['sbr_id' => Auth::user()->subscriber->sbr_id]);
        return $rows;*/

        try {
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $earnList = DB::table('cashout')
                ->join('cashout_status', 'cashout_status.cos_id', '=', 'cashout.co_status')
                ->where('co_bef_id', $sbr_id)
                ->where('co_type', 1)
                ->select('cashout.*', 'cashout_status.cos_name')
                ->orderBy('co_date_created', 'DESC')
                ->paginate(5);

            return $earnList;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function test(Request $request)
    {
        DB::transaction(function () {
            if (Auth::User()) {
                $model = Subscriber::lockForUpdate()->find(1);
                sleep(30);
                $model->point = 100000;
                $model->save();
            } else {
                $model = Subscriber::lockForUpdate()->find(1);
                $model->point = $model->point + 1;
                $model->save();
            }

            return $model;
        });

    }

    public function test2()
    {
        $sbr_id = Auth::user()->subscriber->sbr_id;

        return dd(DB::transaction(function () use ($sbr_id) {

            $updated = 0;
            do {
                try {
                    $model = Subscriber::lockForUpdate()->where('sbr_id', $sbr_id)->first();
                    if (($model->sbr_earning - 50) < 0) {
                        $updated = 1;
                        return "Not Ok $model->sbr_earning";

                    }
                    $model->sbr_earning = $model->sbr_earning - 50;

                    $model->save();
                    $updated = 1;
                    //sleep(10);
                } catch (\Exception $x) {

                }

            } while (!$updated);


            /*if (($model->sbr_earning - 50) < 0) {
                return "Not Ok";
            }*/
            //sleep(30);
            //$model->point = 100000;
            //$model->save();
            return "Ok $model->sbr_earning";
            //return $sbr_earning;
        }));

        //sleep(30);
        //$model->point = 100000;
        //$model->save();

        //return $sbr_earning;

        //return $sbr_earning;
    }

    public function test3()
    {
        $sbr_id = Auth::user()->subscriber->sbr_id;
        //$sbr_earning = Auth::user()->subscriber->sbr_earning;
        //$sbr_earning = 0;

        return dd(DB::transaction(function () use ($sbr_id) {

            $model = Subscriber::lockForUpdate()->where('sbr_id', $sbr_id)->first();
            sleep(10);
            $model->sbr_earning = 100;
            $model->save();


            //return $sp;
            return $model->sbr_earning;
        }));


        //return $sbr_earning;
    }

    public function postSubscriberCashoutDetails(Request $request)
    {

        $sbr_id = Auth::user()->subscriber->sbr_id;
        $sbr_earning = Auth::user()->subscriber->sbr_earning;
        $sbr_cashout_amount = $request['sbr_cashout_amount'];
        $sbr_balance = ($sbr_earning - $sbr_cashout_amount);


        $bank_id = $request['sbr_bank'];

        $bankswiftcode = DB::table('bank')
            ->where('bank_id', $bank_id)
            ->get();

        foreach ($bankswiftcode as $swiftcode) {
            $code = $swiftcode->bank_swiftcode;
        }

        //$pet['co_id'] = Input::get('apm_expect_price_sbh');
        $pet['co_bef_id'] = $sbr_id;
        $pet['co_amount'] = $sbr_cashout_amount;
        $pet['co_bank_id'] = $request['sbr_bank'];
        $pet['co_bank_accholder'] = $request['sbr_bank_accholder'];
        $pet['co_bank_accicno'] = $request['sbr_bank_ic'];
        $pet['co_bank_acc'] = $request['sbr_bank_no'];
        $pet['co_bank_postcode'] = $request['sbr_postcode'];

        $pet['co_swiftcode'] = $code;
        $pet['co_date_created'] = date('Y-m-d H:i:s');
        //$pet['co_date_modified'] = Input::get('apm_expect_final_price_sbh');
        //$pet['co_date_to_bank'] = Input::get('apm_expect_final_price_swk');

        //$pet['co_notes'] = Input::get('apm_expect_final_price_sem');
        $pet['co_type'] = 1;
        //$pet['co_active'] = Input::get('apm_expect_final_price_swk');

        if ($sbr_cashout_amount <= $sbr_earning && $sbr_balance >= 0 && $sbr_cashout_amount !== null) {
            $cashout = new Cashout();
            $cashout->fill($pet);
            $cashout->save();

            //return redirect(patchSubscriberEarning());
            return $this->patchSubscriberEarning($request);
        } else {
            return redirect('/home?cashout_fail=true');
        }

    }

    public function patchSubscriberEarning(Request $request)
    {

        $sbr_id = Auth::user()->subscriber->sbr_id;
        $sbr_earning = Auth::user()->subscriber->sbr_earning;
        $sbr_cashout_amount = $request['sbr_cashout_amount'];
        $sbr_balance = ($sbr_earning - $sbr_cashout_amount);

        DB::transaction(function () use ($request, $sbr_balance, $sbr_cashout_amount, $sbr_earning, $sbr_id) {
            if (Auth::User()) {
                $sp = Subscriber::lockForUpdate()->findOrFail($sbr_id);

                $sp->sbr_modified_date = date('Y-m-d H:i:s');
                //$sp->spt_sbrId = Auth::user()->subscriber->sbr_id;
                $sp->sbr_earning = $sbr_balance;
                $sp->sbr_bank_accno = $request['sbr_bank_no'];
                $sp->sbr_bank_acc_holder = $request['sbr_bank_accholder'];
                $sp->sbr_bank_accicno = $request['sbr_bank_ic'];
                $sp->sbr_bankId = $request['sbr_bank'];
                $sp->sbr_zipcode = $request['sbr_postcode'];
                $sp->sbr_city = $request['sbr_city'];
                $sp->sbr_state = $request['sbr_state'];

                if ($sbr_cashout_amount <= $sbr_earning && $sbr_balance >= 0 && $sbr_cashout_amount !== null) {
                    $sp->save();
                    //Session::flash('cashout_success', 'true');
                    //$request->session()->flash('cashout_success', 'true');
                    return redirect('/home?cashout_success=true');
                } else {
                    return redirect('/home?cashout_fail=true');
                }
            } else {

            }

        });


    }
}
