<?php

/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Config;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function getLang(Request $request)
    {
        $userlang = auth()->user();
        $user_lang = $userlang->user_lang;
        $curr_lang = $request->session()->get('locale');

        $fallback_locale = Config::get('app.fallback_locale');
        //return $request->session()->get('locale');
        if ($curr_lang == null) {
            $user1 = auth()->user();
            $id = $user1->user_id;
            $user = User::findOrFail($id);
            $user->user_lang = $user_lang;
            $user->update();

            \Session::put('locale', $user_lang);

            return response()->json($curr_lang);
        } else if ($user_lang == null && $curr_lang == null) {
            $user1 = auth()->user();
            $id = $user1->user_id;
            $user = User::findOrFail($id);
            $user->user_lang = $fallback_locale;
            $user->update();

            \Session::put('locale', $fallback_locale);

            return response()->json($fallback_locale);

        } else {
            return response()->json($curr_lang);
        }

    }

    public function switchLang(Request $request)
    {
        $lang = $request->lang;
        $user1 = auth()->user()->user_id;
        $id = $user1;

        $user = User::findOrFail($id);


        $user->user_lang = $lang;
        \Session::put('locale', $lang);
        //session(['applocale' => $lang]);
        //$request->session()->put('locale', $lang);
        //$ff = "bm";
        \App::setLocale($lang);
        $user->update();

        //return redirect()->to('/home');
        //return \Redirect::back();
    }

    public function languages()
    {
        //
        //$xx = \App::getLocale();
        //dd($xx);
        //return ;
        //return response()->json(State::latest()->get());
        return Config::get('app.languages');
        //return \App::getLocale();
        //return $request->session()->get('locale');


        //return State::all();
    }

    public function fbpost()
    {

    }
}
