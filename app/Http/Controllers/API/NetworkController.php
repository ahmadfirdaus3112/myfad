<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers\API;

use App\Merchant;
use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class NetworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getNetwork(Request $request)
    {

        $user = Auth::user()->toArray();
        $subscriber = Subscriber::find($user['user_subscriber_id']);
        //$subscriber=$user->subscriber->sbr_id;
        //print $subscriber;
        //dd($subscriber);
        //$merchants = $this->getNetworkMerchant(1, true);

        $data['system_URL'] = config('app.url');
        $data['myfad_URL'] = config('myfad.url');
        $data['user_hash'] = $user['user_hash'];
        //$data['level1'] = $L1['list'];

        //$data['merchantList'] = $merchants['list'];
        //$data['merchantCount'] = $merchants['count'];
        $whatsAppLink = $data['system_URL'] . "/promoter.php?hash=" . $user['user_hash'];
        $whatsAppLink = urlencode($whatsAppLink);
        $data['whatsAppLink'] = $whatsAppLink;

        //$data['session']=$user;
        $result = $data;
        print json_encode($result);
        exit();
    }

    public function getNetworkLevel1()
    {
        $lvl1 = Subscriber::where('sbr_upline1', Auth::user()->subscriber->sbr_id)->paginate(10);
        //dump($lvl1);
        return $lvl1;

    }

    public function getNetworkLevel2()
    {
        $lvl2 = Subscriber::where('sbr_upline2', Auth::user()->subscriber->sbr_id)->paginate(10);
        //dump($lvl1);
        return $lvl2;
    }

    public function getNetworkMerchant()
    {
        $promoter = Merchant::where('mct_promoter', Auth::user()->subscriber->sbr_id)->paginate(10);
        //dump($lvl1);
        return $promoter;

    }

    public function updateMsg(Request $request)
    {
        try {
            $sbr_msg = $request['sbr_msg'];
            $sbr_id = \auth()->user()->subscriber->sbr_id;

            $sp = Subscriber::findOrFail($sbr_id);

            $sp->sbr_msg = $sbr_msg;

            $sp->update();

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}
