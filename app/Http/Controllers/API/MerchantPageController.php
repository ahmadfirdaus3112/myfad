<?php

namespace App\Http\Controllers\API;

use App\Helpers\Helper;
use App\MerchantPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class MerchantPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mct_id)
    {
        //
        if (in_array($mct_id, Helper::merchant_admin())) {
            return MerchantPage::where('mp_merchant', $mct_id)->paginate(10);
        } else {

            return (['isValid' => false, 'errors' => 'Not Authorized']);

        }
        //return User::latest()->paginate(2);


    }

    public function index2()
    {
        //

        //return User::latest()->paginate(2);
        $user = MerchantPage::where('mp_merchant', Auth::user()->merchant->mct_id)->get();
        return $user;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $mct_id)
    {
        $this->validate($request, [
            'mp_name' => 'required|string|max:191',
            'mp_pageid' => 'required|string|max:191',

        ]);

        return MerchantPage::create([

            'mp_name' => $request['mp_name'],
            'mp_pageid' => $request['mp_pageid'],
            'mp_merchant' => $mct_id,
            'mp_created_by' => \auth()->user()->merchant->mct_email,
            'mp_created_date' => Carbon::now(),


        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MerchantPage $merchantPage
     * @return \Illuminate\Http\Response
     */
    public function show(MerchantPage $merchantPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\MerchantPage $merchantPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = MerchantPage::findOrFail($id);

        $this->validate($request, [
            'mp_name' => 'required|string|max:191',
            'mp_pageid' => 'required|string|max:191',

        ]);

        $page->update($request->all());

        return ['message' => 'Updated page info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MerchantPage $merchantPage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //$this->authorize('isAdmin');

        $page = MerchantPage::findOrFail($id);
        // delete the page

        $page->delete();

        return ['message' => 'Page Deleted'];
    }
}
