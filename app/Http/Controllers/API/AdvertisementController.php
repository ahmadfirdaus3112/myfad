<?php

namespace App\Http\Controllers\API;

use App\Advertisement;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;


class AdvertisementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mct_id)
    {
        //
        if (in_array($mct_id, Helper::merchant_admin())) {
            $user = Advertisement::where('ads_merchant', $mct_id)->where('ads_type', 1)->orderBy('ads_created_date', 'DESC')->with('status')->withCount('share_progress')->withCount('credits')->paginate(10);
            return $user;
        } else {
            return (['isValid' => false, 'errors' => 'Not Authorized']);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$mct_id)
    {
        $this->validate($request, [

            'ads_url' => 'required',
            'ads_title' => 'required|string|max:191',

            'states_checked' => 'required',
            'ages_checked' => 'required',

            'genders_checked' => 'required',
            'races_checked' => 'required',
            'religions_checked' => 'required',
            'educations_checked' => 'required',
            'occupations_checked' => 'required',
            'incomes_checked' => 'required',
            'interests_checked' => 'required|size:10',

        ]);

        $data = array(
            'state' => $request['states_checked'],
            'age' => $request['ages_checked'],

            'gender' => $request['genders_checked'],
            'race' => $request['races_checked'],
            'religion' => $request['religions_checked'],
            'education' => $request['educations_checked'],
            'occupation' => $request['occupations_checked'],
            'income' => $request['incomes_checked'],
            'interest' => $request['interests_checked'],
        );
        $rr = json_encode($data);
        //$demo = str_replace(array('[', ']'), '"', $rr);

        //dump($data);
        $ads = Advertisement::create([
            'ads_merchant' => \auth()->user()->user_merchant_id,
            'ads_created_by' => \auth()->user()->merchant->mct_email,
            'ads_created_date' => Carbon::now(),
            'ads_status' => 1,
            'ads_title' => $request['ads_title'],
            'ads_url' => $request['ads_url'],
            'ads_keyword' => $request['ads_keyword'],
            'ads_demographics' => $rr,


        ]);
        $insertedId = $ads->ads_id;
        $token_limit = $request['token_use'];
        //dump($insertedId);
        $this->updateCredits($mct_id,$insertedId,$token_limit);

        return $ads;


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement $advertisement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $advertisement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Advertisement::findOrFail($id);
        // delete the page

        $page->delete();

        return ['message' => 'Ads Deleted'];
    }
    public function updateCredits($mct_id,$insertedId,$token_limit)
    {
        //
        //dump($insertedId);
        return Token::where('credit_merchant',$mct_id)->where('credit_used',0)->where('credit_ads_id',null)->limit($token_limit)->update([
            'credit_ads_id' => $insertedId,
            'credit_used' => 1,
        ]);

        //return $this->getCredits($mct_id);

    }
}
