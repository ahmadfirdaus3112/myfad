<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ShareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPendingShares(Request $request)
    {
        $data = array();
        $result = array();
        $data = $this->getSelectedShare(true);
        $result['SelectedShareCount'] = $data['count'];

        $data = $this->getContestShare(true);
        $result['ContestShareCount'] = $data['count'];

        $data = $this->getOpenShare(true);
        $result['OpenShareCount'] = $data['count'];

        $data = $this->getCharityShare(true);
        $result['CharityShareCount'] = $data['count'];
        $data['count'] = 1;

        $reply[] = $result;
        return $reply;
        //exit();
    }

    public function getSelectedShare($returnValue = false)
    {
        $qs = "SELECT * FROM subscriber_post 
		INNER JOIN advertisement  AS advertisement ON (advertisement.ads_id=subscriber_post.spt_adsId) 
		WHERE spt_fbPostId IS NULL 
		AND spt_sbrId=:sbr_id
		AND spt_status=2 
		AND spt_expiry_date >= NOW()
		AND spt_type IN(0,1) 
		ORDER BY spt_postdate ASC";
        $rows = DB::select(DB::raw($qs), ['sbr_id' => \auth()->user()->subscriber->sbr_id]);

        $result = array();
        foreach ($rows as $row) {
            $url2 = $row->ads_url;
            $result[] = ['id' => $row->ads_id
                , 'ads_title' => $row->ads_title
                , 'spt_id' => $row->spt_id
                , 'fbPostURL' => $url2
                , 'ads_post_id' => $row->ads_post_id];
        }

        if ($returnValue === true) {
            $data['list'] = $result;
            if ($result == "") {
                $data['count'] = 0;
            } else {
                $data['count'] = count($result);
            }
            return $data;
        }


        return $result;
    }

    public function getContestShare($returnValue = false)
    {
        $qs = "SELECT * FROM advertisement WHERE  ads_status IN(5) AND ads_type=3 AND ads_id not In (
			SELECT spt_adsId FROM subscriber_post_contest WHERE spt_status NOT IN (4,5,6) 
			AND spt_sbrId = :sbr_id 
			And spt_fbPostId IS NOT NULL
			AND spt_postdate IS NOT NULL
			GROUP BY spt_adsId
		)";
        $rows = DB::select(DB::raw($qs), ['sbr_id' => \auth()->user()->subscriber->sbr_id]);

        //$rows = R::getAll ( $qs,[$subscriber['sbr_id']] );
        $result = array();
        foreach ($rows as $row) {
            $url2 = $row->ads_url;
            $result[] = ['id' => $row->ads_id
                , 'ads_title' => $row->ads_title
                , 'fbPostURL' => $url2
                , 'ads_post_id' => $row->ads_post_id];
        }
        if ($returnValue === true) {
            $data['list'] = $result;
            if ($result == "") {
                $data['count'] = 0;
            } else {
                $data['count'] = count($result);
            }
            return $data;
        }

        return $result;

    }

    public function getOpenShare($returnValue = false)
    {
        $qs = "SELECT * FROM advertisement 
		 WHERE ads_open_share_balance > 0 AND ads_status IN(7) AND ads_id NOT IN (
			SELECT spt_adsId FROM subscriber_post WHERE spt_status NOT IN (4,5,6) 
			AND spt_sbrId = :sbr_id
			AND spt_fbPostId IS NOT NULL
			AND spt_postdate IS NOT NULL
			GROUP BY spt_adsId
		)";
        $rows = DB::select(DB::raw($qs), ['sbr_id' => \auth()->user()->subscriber->sbr_id]);
        //print $qs;
        //print $subscriber['sbr_id'];

        $result = array();
        foreach ($rows as $row) {
            //dd($row);
            $url2 = $row->ads_url;
            $result[] = ['id' => $row->ads_id, 'ads_title' => $row->ads_title, 'fbPostURL' => $url2, 'ads_post_id' => $row->ads_post_id];
        }
        if ($returnValue === true) {
            $data['list'] = $result;
            if ($result == "") {
                $data['count'] = 0;
            } else {
                $data['count'] = count($result);
            }
            return $data;
        }
        return $result;

    }

    public function getCharityShare($returnValue = false)
    {
        $qs = "SELECT *,(
						SELECT MAX(spt_postdate) 
						FROM subscriber_post 
						WHERE spt_sbrId=:sbr_id AND spt_adsId=ads_id) AS spt_postdate,
						IF(ads_affiliate=0,'myfAD News',affiliate_name) AS affiliate_name
						FROM advertisement 
				LEFT JOIN affiliate ON(affiliate.affiliate_id=advertisement.ads_affiliate)
				WHERE ads_type = 4 AND ads_status=5 AND ads_endtime >= NOW() 
				AND  ads_id NOT IN
				(SELECT spt_adsId FROM subscriber_post WHERE spt_fbPostId IS NOT NULL AND spt_postdate IS NOT NULL
				AND DATEDIFF(CURDATE(),spt_postdate) <1 AND spt_sbrId=:sbr_id2)";

        $rows = DB::select(DB::raw($qs), ['sbr_id' => \auth()->user()->subscriber->sbr_id, 'sbr_id2' => \auth()->user()->subscriber->sbr_id]);
        $result = array();
        foreach ($rows as $row) {
            //dd($row);
            $url2 = $row->ads_url;
            $result[] = ['id' => $row->ads_id, 'ads_title' => $row->ads_title, 'fbPostURL' => $url2, 'ads_post_id' => $row->ads_post_id];
        }
        if ($returnValue === true) {
            $data['list'] = $result;
            if ($result == "") {
                $data['count'] = 0;
            } else {
                $data['count'] = count($result);
            }
            return $data;
        }

        return $result;
    }
}
