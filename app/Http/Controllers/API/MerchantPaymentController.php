<?php

namespace App\Http\Controllers\API;

use App\Merchant;
use App\MerchantAdmin;
use App\MerchantPayment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper;

class MerchantPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTokenPurchaseHistory($mct_id)
    {
        if (in_array($mct_id, Helper::merchant_admin())) {
            return MerchantPayment::where('mp_merchant_id', $mct_id)->where('mp_status', 1)->orderBy('mp_created_date', 'DESC')->paginate(10);
        } else {

            return (['isValid' => false, 'errors' => 'Not Authorized']);

        }

    }

}
