<?php

namespace App\Http\Controllers\API;

use App\Merchant;
use App\MerchantAdmin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MerchantAdministratorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $merchant_ori = User::where('user_id', auth()->user()->user_id)->pluck('user_merchant_id');
        $merchant_new = MerchantAdmin::where('ma_user_id', auth()->user()->user_id)->pluck('ma_merchant_id');

        $collection = collect($merchant_ori);

        $merged = $collection->merge($merchant_new)->unique();

        $merchant = Merchant::whereIn('mct_id', $merged)->with('merchant_admin')->paginate(5);

        return $merchant;
        //return MerchantAdmin::with('merchant')->where('ma_user_id', \auth()->user()->user_id)->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $merchant = User::where('user_username', $id)->first();
        //dd($merchant);
        $ma = MerchantAdmin::where('ma_merchant_id', $merchant->user_merchant_id)->first();

        if ($id == '') {
            return (['isValid' => false, 'errors1' => 'Please insert email']);

        } else if ($id == \auth()->user()->user_username) {

            return ['isValid' => false, 'errors' => 'Cannot Add Own Account'];
        } else if (!$ma) {
            MerchantAdmin::create([
                'ma_merchant_id' => $merchant->user_merchant_id,
                'ma_user_id' => \auth()->user()->user_id,

            ]);

            return ['message' => 'Add Admin'];
        } else {
            return (['isValid' => false, 'errors' => 'Already add Admin']);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $tg = MerchantAdmin::where('ma_merchant_id', $id);

        if ($id == auth()->user()->user_merchant_id) {
            return ['isValid' => false, 'errors' => 'Cannot Remove Own Account'];

        } else {
            $tg->delete();

            return ['message' => 'Updated page info'];
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
