<?php

namespace App\Http\Controllers\API;

use App\Age;
use App\Education;
use App\Gender;
use App\Http\Controllers\Controller;
use App\Income;
use App\Interest;
use App\Merchant;
use App\MerchantAdmin;
use App\Occupation;
use App\Race;
use App\Religion;
use App\State;
use App\User;
use Helper;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mct_id)
    {
        //
        //$mct_id = \auth()->user()->merchant->mct_id;

        if (in_array($mct_id, Helper::merchant_admin())) {

            return Merchant::find($mct_id);

        } else {

            return (['isValid' => false, 'errors' => 'Not Authorized']);

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Merchant $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Merchant $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mct = Merchant::findOrFail($id);

        $this->validate($request, [
            'mct_name' => 'required|string|max:191',
            'mct_email' => 'required|string|max:191',

        ]);

        $mct->update($request->all());

        return ['message' => 'Updated page info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Merchant $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchant $merchant)
    {
        //
    }

    public function states()
    {
        return State::all('state_id', 'state_name');
    }

    public function ages()
    {
        return Age::all('age_id', 'age_name');
    }

    public function genders()
    {
        return Gender::all();
    }

    public function races()
    {
        return Race::all();
    }

    public function religions()
    {
        return Religion::all();
    }

    public function educations()
    {
        return Education::all();
    }

    public function occupations()
    {
        return Occupation::all();
    }

    public function incomes()
    {
        return Income::all();
    }

    public function interests()
    {
        $interest = Interest::all('it_id', 'it_name', 'it_desc');

        return $interest;
    }

    public function getTokenCount($mct_id)
    {
        $merchant_ori = User::where('user_id', auth()->user()->user_id)->pluck('user_merchant_id');
        $merchant_new = MerchantAdmin::where('ma_user_id', auth()->user()->user_id)->pluck('ma_merchant_id');

        $collection = collect($merchant_ori);

        $merged = $collection->merge($merchant_new)->unique()->toArray();

        $merchant = Merchant::whereIn('mct_id', $merged)->get();

        //dd($merged);
        if (in_array($mct_id, $merged)) {
            // execute success
            return Merchant::where('mct_id', $mct_id)->first();
        } else {
            // execute fail
            abort(403, "Sorry, you are not authorized to access this page :(");
            //return response()->view('errors.500', [], 500);
        }

    }
}
