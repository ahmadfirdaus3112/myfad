<?php
/**
 * Copyright (c) myfAd Tech Sdn. Bhd.
 * Created by @ahmadfirdaus3112@gmail.com
 */

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $guarded = [];

    public $timestamps = false;

    protected $table = 'user';
    protected $guarded = ['user_pwd'];
    protected $primaryKey = 'user_id';
    /*protected $fillable = [
        'user_username'
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pwd', 'remember_token',
    ];

    public function subscriber()
    {
        return $this->belongsTo(Subscriber::class, 'user_subscriber_id', 'sbr_id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'user_merchant_id', 'mct_id');
    }

    public function merchant_admin()
    {
        //return $this->belongsTo(MerchantAdmin::class, 'user_id', 'ma_user_id');
        return $this->hasMany(MerchantAdmin::class, 'user_id', 'ma_user_id');
    }
}
