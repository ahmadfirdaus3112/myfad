<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = 'race_id';
    protected $table = 'race';
}
