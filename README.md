## Installation


* Clone the repo ` git clone https://ahmadfirdaus3112@bitbucket.org/ahmadfirdaus3112/myfad.git `
* `cd ` to project folder. 
* Run ` composer install `
* Save as the `.env.example` to `.env` and set your database information 
* Run ` php artisan key:generate` to generate the app key
* Run ` php artisan passport:install` to install laravel passport
* Run ` php artisan migrate` to generate the table or set up DB yourself
* Run ` npm install ` 
* Run ` npm run watch ` to compile the app and start debugging
* Done. Register new user in the app and start login.



