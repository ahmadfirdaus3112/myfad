(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["view-Dashboard-vue"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/Dashboard.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/components/Dashboard.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      SelectedShareCount: "",
      CharityShareCount: "",
      OpenShareCount: "",
      ContestShareCount: "",
      earning: ""
    };
  },
  methods: {
    getEarning: function getEarning() {
      var _this = this;

      axios.get("api/subscriber/earning").then(function (response) {
        return _this.earning = response.data;
      }).catch(function (error) {
        return console.log(error.response.data);
      });
    },
    getCount: function getCount() {
      var _this2 = this;

      axios.get("api/subscriber/share_count").then(function (response) {
        return _this2.SelectedShareCount = response.data[0].SelectedShareCount, _this2.CharityShareCount = response.data[0].CharityShareCount, _this2.OpenShareCount = response.data[0].OpenShareCount, _this2.ContestShareCount = response.data[0].ContestShareCount;
      }).catch(function (error) {
        return console.log(error.response.data);
      });
    }
  },
  created: function created() {
    this.getCount();
    this.getEarning();
  },
  mounted: function mounted() {// Code that will run only after the
    // entire view has been rendered
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/Dashboard.vue?vue&type=template&id=1f65406d&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/components/Dashboard.vue?vue&type=template&id=1f65406d& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "content-header" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row mb-2" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-sm-6" }, [
            _c("ol", { staticClass: "breadcrumb float-sm-right" }, [
              _c(
                "li",
                { staticClass: "breadcrumb-item" },
                [
                  _c("router-link", { attrs: { to: "/home" } }, [
                    _vm._v("Home")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("li", { staticClass: "breadcrumb-item active" }, [
                _vm._v("Dashboard")
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-3 col-6" }, [
        _c("div", { staticClass: "small-box bg-info" }, [
          _c("div", { staticClass: "inner" }, [
            _c("h3", [_vm._v(_vm._s(_vm.SelectedShareCount))]),
            _vm._v(" "),
            _c("p", [_vm._v("Selected Share")])
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "small-box-footer",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  return _vm.$router.push("/subscriber_ads?ads=selected-ads")
                }
              }
            },
            [
              _vm._v("\n\t\t\t\t\t\tMore info\n\t\t\t\t\t\t"),
              _c("i", { staticClass: "fa fa-arrow-circle-right" })
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-3 col-6" }, [
        _c("div", { staticClass: "small-box bg-success" }, [
          _c("div", { staticClass: "inner" }, [
            _c("h3", [_vm._v(_vm._s(_vm.CharityShareCount))]),
            _vm._v(" "),
            _c("p", [_vm._v("Charity Share")])
          ]),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "small-box-footer",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  return _vm.$router.push("/subscriber_ads?ads=charity-ads")
                }
              }
            },
            [
              _vm._v("\n\t\t\t\t\t\tMore info\n\t\t\t\t\t\t"),
              _c("i", { staticClass: "fa fa-arrow-circle-right" })
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-3 col-6" }, [
        _c("div", { staticClass: "small-box bg-warning" }, [
          _c("div", { staticClass: "inner" }, [
            _c("h3", [_vm._v(_vm._s(_vm.OpenShareCount))]),
            _vm._v(" "),
            _c("p", [_vm._v("Open Share")])
          ]),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "small-box-footer",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  return _vm.$router.push("/subscriber_ads?ads=open-ads")
                }
              }
            },
            [
              _vm._v("\n\t\t\t\t\t\tMore info\n\t\t\t\t\t\t"),
              _c("i", { staticClass: "fa fa-arrow-circle-right" })
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-3 col-6" }, [
        _c("div", { staticClass: "small-box bg-danger" }, [
          _c("div", { staticClass: "inner" }, [
            _c("h3", [_vm._v(_vm._s(_vm.ContestShareCount))]),
            _vm._v(" "),
            _c("p", [_vm._v("Open Contest")])
          ]),
          _vm._v(" "),
          _vm._m(4),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "small-box-footer",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  return _vm.$router.push("/subscriber_ads?ads=contest-ads")
                }
              }
            },
            [
              _vm._v("\n\t\t\t\t\t\tMore info\n\t\t\t\t\t\t"),
              _c("i", { staticClass: "fa fa-arrow-circle-right" })
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col" }, [
        _c(
          "div",
          {
            staticClass: "small-box bg-primary-gradient",
            attrs: { align: "center" }
          },
          [
            _c("div", { staticClass: "inner" }, [
              _c("h3", [_vm._v("RM" + _vm._s(_vm.earning))]),
              _vm._v(" "),
              _c("p", [_vm._v("Earning")])
            ]),
            _vm._v(" "),
            _vm._m(5),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "small-box-footer",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    return _vm.$router.push("/subscriber_cashout")
                  }
                }
              },
              [
                _vm._v("\n\t\t\t\t\t\tCashout\n\t\t\t\t\t\t"),
                _c("i", { staticClass: "fa fa-arrow-circle-right" })
              ]
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("h1", { staticClass: "m-0 text-dark" }, [_vm._v("Dashboard")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "icon" }, [
      _c("i", { staticClass: "fas fa-envelope" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "icon" }, [
      _c("i", { staticClass: "fas fa-hand-holding-usd" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "icon" }, [
      _c("i", { staticClass: "fas fa-envelope-open-text" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "icon" }, [
      _c("i", { staticClass: "fas fa-trophy" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "icon" }, [
      _c("i", { staticClass: "fas fa-money-bill-wave" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/components/Dashboard.vue":
/*!******************************************************!*\
  !*** ./resources/assets/js/components/Dashboard.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_1f65406d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=1f65406d& */ "./resources/assets/js/components/Dashboard.vue?vue&type=template&id=1f65406d&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/assets/js/components/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_1f65406d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_1f65406d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/components/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/components/Dashboard.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/assets/js/components/Dashboard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/components/Dashboard.vue?vue&type=template&id=1f65406d&":
/*!*************************************************************************************!*\
  !*** ./resources/assets/js/components/Dashboard.vue?vue&type=template&id=1f65406d& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_1f65406d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=1f65406d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/Dashboard.vue?vue&type=template&id=1f65406d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_1f65406d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_1f65406d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);